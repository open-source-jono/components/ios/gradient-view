//
//  GradientView.swift
//  GradientViewApp
//
//  Created by Jonathan Unsworth on 2018/09/13.
//  Copyright © 2018 Jonathan Unsworth. All rights reserved.
//

import UIKit

@IBDesignable class GradientView: UIView {

    @IBInspectable var startColor: UIColor = UIColor(red: 65/255, green: 150/255, blue: 255/255, alpha: 1) {
        didSet {
            
        }
    }
    
    @IBInspectable var endColor: UIColor = UIColor(red: 120/255, green: 120/255, blue: 0/255, alpha: 1) {
        didSet {
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience public init(frame: CGRect, startColor: UIColor, endColor: UIColor) {
        self.init(frame: frame)
        self.startColor = startColor
        self.endColor = endColor
    }
    
     /*Only override draw() if you perform custom drawing.
     An empty implementation adversely affects performance during animation.*/
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        // 1 Get current graphics context
        guard let currentContext = UIGraphicsGetCurrentContext() else {
            return
        }
        
        // 2 Save graphics state
        currentContext.saveGState()
        
        // 3 Create RGB color space
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        // 4 Generate color components
        let startColor = self.startColor
        guard let startColorComponents = startColor.cgColor.components else { return }
        
        let endColor = self.endColor
        guard let endColorComponents = endColor.cgColor.components else { return }
        
        // 5 Setup array with color components
        let colorComponents: [CGFloat]
            = [startColorComponents[0], startColorComponents[1], startColorComponents[2], startColorComponents[3], endColorComponents[0], endColorComponents[1], endColorComponents[2], endColorComponents[3]]
        
        // 6 Set locations
        let locations:[CGFloat] = [0.0, 1.0]
        
        // 7 Generate gradient with color components
        guard let gradient = CGGradient(colorSpace: colorSpace,colorComponents: colorComponents,locations: locations,count: 2) else { return }
        
        let startPoint = CGPoint(x: 0, y: self.bounds.height)
        let endPoint = CGPoint(x: self.bounds.width,y: self.bounds.height)
        
        // 8 Draw linear gradient
        currentContext.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: UInt32(0)))
        
        // 9 Restore graphics state
        currentContext.restoreGState()
    }

}
